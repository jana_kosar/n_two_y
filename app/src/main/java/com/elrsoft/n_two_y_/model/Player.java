package com.elrsoft.n_two_y_.model;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.Stack;

/**
 * Created by yana on 18.06.15.
 */
public class Player extends org.andengine.entity.sprite.Sprite{
    private int mWeight;
    private Stack mStack; //this represents the stack that this ring belongs to
    private Sprite shadow;

    public Player(int weight, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        this.mWeight = weight;
    }

    public int getmWeight() {
        return mWeight;
    }
    public Stack getmStack() {
        return mStack;
    }
    public void setmStack(Stack mStack) {
        this.mStack = mStack;
    }
    public Sprite getShadow() {
        return shadow;
    }
    public void setShadow(Sprite shadow) {
        this.shadow = shadow;
    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);
    }
}
