package com.elrsoft.n_two_y_.model;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.controller.SpriteController;
import com.elrsoft.n_two_y_.controller.WallController;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;

import java.util.Stack;

/**
 * Created by yana on 19.07.15.
 */
public class WallShadow extends Sprite{

    private int mWeight;
    private Stack mStack; //this represents the stack that this ring belongs to
    private Sprite mTower;
    private MainActivity activity;
    private Scene scene;

    public WallShadow(int weight, float pX, float pY, ITextureRegion pTextureRegion,  Scene scene, MainActivity activity) {
        super(pX, pY, pTextureRegion, activity.getVertexBufferObjectManager());
        this.mWeight = weight;
        this.activity = activity;
        this.scene = scene;
    }

    public int getmWeight() {
        return mWeight;
    }

    public Stack getmStack() {
        return mStack;
    }

    public void setmStack(Stack mStack) {
        this.mStack = mStack;
    }

    public Sprite getmTower() {
        return mTower;
    }

    public void setmTower(Sprite mTower) {
        this.mTower = mTower;
    }

    @Override
    public void onManagedUpdate(float pSecondsElapsed) {

        if(ResourceManager.getInstance().isTouch()){
            this.setPosition(this.getX(),
                    this.getY() + 2);}


        if(this.getY() > ResourceManager.getInstance().getHeight()) {
            SpriteController.removeSprite(this, activity);
            WallController.updateShadow(activity, scene);

            scene.detachChild(ResourceManager.getInstance().getWall());
            scene.attachChild(ResourceManager.getInstance().getWall());
        }


    }
}
