package com.elrsoft.n_two_y_.model;

import android.util.Log;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.controller.SpriteController;
import com.elrsoft.n_two_y_.controller.WallController;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.Stack;

/**
 * Created by nazar on 04.07.15.
 */
public class Wall extends Obstacle{

    private int mWeight;
    private Stack mStack; //this represents the stack that this ring belongs to
    private Sprite mTower;
    private MainActivity activity;
    private  Scene scene;
    private boolean isMoving = true;

    public Wall(int weight, float pX, float pY, ITextureRegion pTextureRegion, MainActivity activity, Scene scene) {
        super(pX, pY, pTextureRegion, activity.getVertexBufferObjectManager());
        this.mWeight = weight;
        this.activity = activity;
        this.scene = scene;
    }

    public int getmWeight() {
        return mWeight;
    }

    public Stack getmStack() {
        return mStack;
    }

    public void setmStack(Stack mStack) {
        this.mStack = mStack;
    }

    public Sprite getmTower() {
        return mTower;
    }

    public void setmTower(Sprite mTower) {
        this.mTower = mTower;
    }


    public boolean isMoving() {
        return isMoving;
    }

    public void setIsMoving(boolean isMoving) {
        this.isMoving = isMoving;
    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {

        if(ResourceManager.getInstance().isTouch()) {
            this.setPosition(this.getX(),
                    this.getY() + 2);
        }


        if(this.getY() >= ResourceManager.getInstance().getHeight() - ResourceManager.getInstance().getHeight()/4
                && this.getY() < ResourceManager.getInstance().getHeight() - ResourceManager.getInstance().getHeight()/4 + 2){
            Log.d("wallStatus", "UPDATE");
            WallController.updateWall(activity, scene);
        }


        if(this.getY() > ResourceManager.getInstance().getHeight()) {
            SpriteController.removeSprite(this, activity);
//            WallController.updateWall(activity, scene);
        }
    }
}
