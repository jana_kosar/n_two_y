package com.elrsoft.n_two_y_.model;

import android.content.Intent;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.activity.WindowActivity;
import com.elrsoft.n_two_y_.controller.SpriteController;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.entity.shape.IShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.Stack;

/**
 * Created by yana on 18.06.15.
 */
public class Block extends Obstacle{

    private int mWeight;
    private Stack mStack; //this represents the stack that this ring belongs to
    private Sprite mTower;
    MainActivity activity;

    public Block(int weight, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, MainActivity activity) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        this.mWeight = weight;
        this.activity = activity;

    }

    public int getmWeight() {
        return mWeight;
    }

    public Stack getmStack() {
        return mStack;
    }

    public void setmStack(Stack mStack) {
        this.mStack = mStack;
    }

    public Sprite getmTower() {
        return mTower;
    }

    public void setmTower(Sprite mTower) {
        this.mTower = mTower;
    }

    @Override
    public void onManagedUpdate(float pSecondsElapsed) {
            this.setPosition(this.getX(),
                    this.getY() + 2);


        if(this.getY() > ResourceManager.getInstance().getHeight()) {
            SpriteController.removeSprite(this, activity);
        }

        if(this.collidesWith(ResourceManager.getInstance().getLeftPlayer()) || this.collidesWith(ResourceManager.getInstance().getRightPlayer())) {
            Intent i = new Intent(activity, WindowActivity.class);
            i.putExtra("fragment", 2);
            activity.startActivity(i);
        }



    }

    @Override
    public boolean collidesWith(IShape pOtherShape) {
        Player p = (Player) pOtherShape;
        float w = ResourceManager.getInstance().getPlayerWSize();
        float h = ResourceManager.getInstance().getPlayerHSize();
        float wb = ResourceManager.getInstance().getBlockWSize();
        float hb = ResourceManager.getInstance().getBlockHSize();
        return ((p.getX() > this.getX() - w + 30 && p.getX() < this.getX() + wb - 30) && (p.getY() + h < this.getY() + hb + 20 && p.getY() + h > this.getY())) ? true : false;
    }
}
