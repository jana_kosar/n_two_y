package com.elrsoft.n_two_y_.model;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.Stack;

/**
 * Created by nazar on 04.07.15.
 */
public class Obstacle extends org.andengine.entity.sprite.Sprite{

    public Obstacle(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
    }

    public int getmWeight(){
        return 0;
    }

    public Stack getmStack(){
        return null;
    }

    public void setmStack(Stack mStack){

    }

    public Sprite getmTower(){
        return null;
    }

    public void setmTower(Sprite mTower){

    }

}
