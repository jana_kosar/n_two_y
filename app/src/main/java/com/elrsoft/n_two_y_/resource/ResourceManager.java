package com.elrsoft.n_two_y_.resource;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.controller.GameResourceController;
import com.elrsoft.n_two_y_.model.Player;
import com.elrsoft.n_two_y_.model.Wall;

import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andlabs.andengine.extension.physicsloader.PhysicsEditorLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by yana on 17.06.15.
 *
 * ResourceManager - singleton-об'єкт, який зберігає всі ресурси, що використовуються в грі
 */
public class ResourceManager {



    private static final ResourceManager RESOURCE_MANAGER = new ResourceManager();


    private ITextureRegion backgroundTextureRegion, firstPlayerTextureRegion, secondPlayerTextureRegion,
            blockTextureRegion, shadowTextureRegion, wallShadowTextureRegion, firstPlayerShadowTextureRegion,
            secondPlayerShadowTextureRegion, wallTextureRegion, riverTextureRegion, rockShadowTextureRegion;
    private Stack stack = new Stack();
    private ITexture backgroundTexture, firstPlayerTexture, secondPlayerTexture, blockTexture, shadowTexture, wallShadow, rockShadow, riverTeture, firstPlayerShadow, secondPlayerShadow, wallTexture;
    private BitmapTextureAtlas bitmapTextureAtlas;
    private int width, height;
    private float playerWSize, playerHSize, blockWSize, blockHSize;
    private Player leftPlayer, rightPlayer;
    private Wall wall;
    private List<ITexture> textures;
    private boolean isTouch;

    private PhysicsWorld physicsWorld;
    private final PhysicsEditorLoader physicsEditorLoader = new PhysicsEditorLoader();

    private MainActivity activity;


    private ResourceManager() {
    }

    public static ResourceManager getInstance() {
        return RESOURCE_MANAGER;
    }

    /**
     * Ініціалізація основних полів, завантаження текстур
     * @param activity
     */
    public void prepare(MainActivity activity) {
        RESOURCE_MANAGER.activity = activity;
        textures = new ArrayList<>();

        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;

        loadGameResources();


    }

    /**
     * Завантаження ігрових текстур
     */
    public void loadGameResources() {

        try {
            GameResourceController.setUpBitmapTextures(activity);
            GameResourceController.loadBitmapTextures(textures);
            GameResourceController.setTextureRegions();

        } catch (IOException e) {

            e.printStackTrace();
        }


    }
    public void loadSplashResources() {
        //TODO implement
    }

    public void unloadSplashResources() {
        //TODO implement
    }

    public void unloadGameResources() {
        //TODO implement
    }


    public ITextureRegion getBackgroundTextureRegion() {
        return backgroundTextureRegion;
    }

    public void setBackgroundTextureRegion(ITextureRegion backgroundTextureRegion) {
        this.backgroundTextureRegion = backgroundTextureRegion;
    }

    public ITextureRegion getFirstPlayerTextureRegion() {
        return firstPlayerTextureRegion;
    }

    public void setFirstPlayerTextureRegion(ITextureRegion firstPlayerTextureRegion) {
        this.firstPlayerTextureRegion = firstPlayerTextureRegion;
    }

    public ITextureRegion getSecondPlayerTextureRegion() {
        return secondPlayerTextureRegion;
    }

    public void setSecondPlayerTextureRegion(ITextureRegion secondPlayerTextureRegion) {
        this.secondPlayerTextureRegion = secondPlayerTextureRegion;
    }

    public ITextureRegion getBlockTextureRegion() {
        return blockTextureRegion;
    }

    public void setBlockTextureRegion(ITextureRegion blockTextureRegion) {
        this.blockTextureRegion = blockTextureRegion;
    }

    public ITextureRegion getShadowTextureRegion() {
        return shadowTextureRegion;
    }

    public void setShadowTextureRegion(ITextureRegion shadowTextureRegion) {
        this.shadowTextureRegion = shadowTextureRegion;
    }

    public ITexture getBackgroundTexture() {
        return backgroundTexture;
    }

    public void setBackgroundTexture(ITexture backgroundTexture) {
        this.backgroundTexture = backgroundTexture;
    }

    public ITexture getFirstPlayerTexture() {
        return firstPlayerTexture;
    }

    public void setFirstPlayerTexture(ITexture firstPlayerTexture) {
        this.firstPlayerTexture = firstPlayerTexture;
    }

    public ITexture getSecondPlayerTexture() {
        return secondPlayerTexture;
    }

    public void setSecondPlayerTexture(ITexture secondPlayerTexture) {
        this.secondPlayerTexture = secondPlayerTexture;
    }

    public Stack getStack() {
        return stack;
    }

    public ITexture getBlockTexture() {
        return blockTexture;
    }

    public void setBlockTexture(ITexture blockTexture) {
        this.blockTexture = blockTexture;
    }

    public ITexture getShadowTexture() {
        return shadowTexture;
    }

    public void setShadowTexture(ITexture shadowTexture) {
        this.shadowTexture = shadowTexture;
    }

    public Player getLeftPlayer() {
        return leftPlayer;
    }

    public void setLeftPlayer(Player leftPlayer) {
        this.leftPlayer = leftPlayer;
    }

    public Player getRightPlayer() {
        return rightPlayer;
    }

    public void setRightPlayer(Player rightPlayer) {
        this.rightPlayer = rightPlayer;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public MainActivity getActivity() {
        return activity;
    }

    public List<ITexture> getTextures() {
        return textures;
    }

    public float getPlayerWSize() {
        return playerWSize;
    }

    public float getPlayerHSize() {
        return playerHSize;
    }

    public void setPlayerSize(float playerWSize, float playerHSize) {
        this.playerWSize = playerWSize;
        this.playerHSize = playerHSize;

    }

    public float getBlockWSize() {
        return blockWSize;
    }

    public float getBlockHSize() {
        return blockHSize;
    }

    public void setBlockSize(float blockWSize, float blockHSize) {
        this.blockWSize = blockWSize;
        this.blockHSize = blockHSize;

    }

    public Wall getWall() {
        return wall;
    }

    public void setWall(Wall wall) {
        this.wall = wall;
    }

    public BitmapTextureAtlas getBitmapTextureAtlas() {
        return bitmapTextureAtlas;
    }

    public void setBitmapTextureAtlas(BitmapTextureAtlas bitmapTextureAtlas) {
        this.bitmapTextureAtlas = bitmapTextureAtlas;
    }

    public ITexture getWallTexture() {
        return wallTexture;
    }

    public void setWallTexture(ITexture wallTexture) {
        this.wallTexture = wallTexture;
    }

    public ITexture getWallShadow() {
        return wallShadow;
    }

    public void setWallShadow(ITexture wallShadow) {
        this.wallShadow = wallShadow;
    }

    public ITextureRegion getWallTextureRegion() {
        return wallTextureRegion;
    }

    public void setWallTextureRegion(ITextureRegion wallTextureRegion) {
        this.wallTextureRegion = wallTextureRegion;
    }

    public ITextureRegion getWallShadowTextureRegion() {
        return wallShadowTextureRegion;
    }

    public void setWallShadowTextureRegion(ITextureRegion wallShadowTextureRegion) {
        this.wallShadowTextureRegion = wallShadowTextureRegion;
    }

    public PhysicsEditorLoader getPhysicsEditorLoader() {
        return physicsEditorLoader;
    }

    public PhysicsWorld getPhysicsWorld() {
        return physicsWorld;
    }

    public boolean isTouch() {
        return isTouch;
    }

    public void setIsTouch(boolean isTouch) {
        this.isTouch = isTouch;
    }

    public ITextureRegion getRiverTextureRegion() {
        return riverTextureRegion;
    }

    public void setRiverTextureRegion(ITextureRegion riverTextureRegion) {
        this.riverTextureRegion = riverTextureRegion;
    }

    public ITexture getRiverTeture() {
        return riverTeture;
    }

    public void setRiverTeture(ITexture riverTeture) {
        this.riverTeture = riverTeture;
    }

    public ITexture getSecondPlayerShadow() {
        return secondPlayerShadow;
    }

    public void setSecondPlayerShadow(ITexture secondPlayerShadow) {
        this.secondPlayerShadow = secondPlayerShadow;
    }

    public ITexture getFirstPlayerShadow() {
        return firstPlayerShadow;
    }

    public void setFirstPlayerShadow(ITexture firstPlayerShadow) {
        this.firstPlayerShadow = firstPlayerShadow;
    }

    public ITextureRegion getSecondPlayerShadowTextureRegion() {
        return secondPlayerShadowTextureRegion;
    }

    public void setSecondPlayerShadowTextureRegion(ITextureRegion secondPlayerShadowTextureRegion) {
        this.secondPlayerShadowTextureRegion = secondPlayerShadowTextureRegion;
    }

    public ITextureRegion getFirstPlayerShadowTextureRegion() {
        return firstPlayerShadowTextureRegion;
    }

    public void setFirstPlayerShadowTextureRegion(ITextureRegion firstPlayerShadowTextureRegion) {
        this.firstPlayerShadowTextureRegion = firstPlayerShadowTextureRegion;
    }

    public ITextureRegion getRockShadowTextureRegion() {
        return rockShadowTextureRegion;
    }

    public void setRockShadowTextureRegion(ITextureRegion rockShadowTextureRegion) {
        this.rockShadowTextureRegion = rockShadowTextureRegion;
    }

    public ITexture getRockShadow() {
        return rockShadow;
    }

    public void setRockShadow(ITexture rockShadow) {
        this.rockShadow = rockShadow;
    }
}
