package com.elrsoft.n_two_y_;

import android.content.Intent;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.activity.WindowActivity;
import com.elrsoft.n_two_y_.controller.SpriteController;
import com.elrsoft.n_two_y_.resource.*;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.Stack;

/**
 * Created by nazar on 08.07.15.
 */
public class Shadow extends org.andengine.entity.sprite.Sprite{

    private int mWeight;
    private Stack mStack; //this represents the stack that this ring belongs to
    private Sprite mTower;
    MainActivity activity;

    public Shadow(int weight, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, MainActivity activity) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        this.mWeight = weight;
        this.activity = activity;
    }

    public int getmWeight() {
        return mWeight;
    }

    public Stack getmStack() {
        return mStack;
    }

    public void setmStack(Stack mStack) {
        this.mStack = mStack;
    }

    public Sprite getmTower() {
        return mTower;
    }

    public void setmTower(Sprite mTower) {
        this.mTower = mTower;
    }

    @Override
    public void onManagedUpdate(float pSecondsElapsed) {
        this.setPosition(this.getX(),
                this.getY() + 6);

        if(this.getY() > com.elrsoft.n_two_y_.resource.ResourceManager.getInstance().getHeight()) {
            SpriteController.removeSprite(this, activity);
        }


    }

}
