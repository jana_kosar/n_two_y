package com.elrsoft.n_two_y_.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentActivity;

import com.elrsoft.n_two_y_.R;
import com.elrsoft.n_two_y_.fragment.EndFragment;
import com.elrsoft.n_two_y_.fragment.MenuFragment;

/**
 * Created by nazar on 07.07.15.
 */
public class WindowActivity extends FragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.window_activity);

//        if(savedInstanceState == null){
//            this.getSupportFragmentManager().beginTransaction().add(R.id.container, new MenuFragment(), "menu").commit();
//
//        } else {

            switch (this.getIntent().getIntExtra("fragment", 1)) {
                case 1:
                    this.getSupportFragmentManager().beginTransaction().add(R.id.container, new MenuFragment(), "menu").commit();
                    break;
                case 2:
                    this.getSupportFragmentManager().beginTransaction().add(R.id.container, new EndFragment(), "end").commit();
                    break;
            }

//        }

    }

}
