package com.elrsoft.n_two_y_.activity;

import com.elrsoft.n_two_y_.controller.EngineOptionsController;
import com.elrsoft.n_two_y_.controller.GameSceneController;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.SimpleBaseGameActivity;

public class MainActivity extends SimpleBaseGameActivity {

    @Override
    protected void onCreateResources() {
        // завантаження всіх ресурсів гри
        ResourceManager.getInstance().prepare(this);
    }

    @Override
    protected Scene onCreateScene() {
        //створення сцени
        return new GameSceneController().onCreateScene(this);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        // налаштування опцій ігрового движка
        return EngineOptionsController.createEngineOptions(this);
    }


}
