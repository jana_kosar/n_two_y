package com.elrsoft.n_two_y_.controller;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.elrsoft.n_two_y_.activity.MainActivity;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;

/**
 * Created by yana on 09.07.15.
 */
public class EngineOptionsController {

    /**
     * Налаштування опцій ігрового движка
     * @param activity
     * @return engineOptions
     */
    public static EngineOptions createEngineOptions(MainActivity activity) {

        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        final Camera camera = new Camera(0, 0, metrics.widthPixels, metrics.heightPixels);


        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
                new RatioResolutionPolicy(metrics.widthPixels, metrics.heightPixels), camera);
        engineOptions.getTouchOptions().setNeedsMultiTouch(true);


        return engineOptions;
    }
}
