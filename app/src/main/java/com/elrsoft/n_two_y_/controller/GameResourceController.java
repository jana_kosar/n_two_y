package com.elrsoft.n_two_y_.controller;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.adt.io.in.IInputStreamOpener;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yana on 09.07.15.
 */
public class GameResourceController {

    /**
     * Ініціалізація ігрових текстур
     * @param activity
     * @throws IOException
     */
    public static void setUpBitmapTextures(final MainActivity activity) throws IOException {



        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        ResourceManager.getInstance().setBitmapTextureAtlas(new BitmapTextureAtlas(
                activity.getTextureManager(), ResourceManager.getInstance().getWidth(),
                ResourceManager.getInstance().getHeight(), TextureOptions.BILINEAR));


        ResourceManager.getInstance().getBitmapTextureAtlas().load();

        ResourceManager.getInstance().setFirstPlayerTexture(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/girl.png");
            }
        }));

        ResourceManager.getInstance().setSecondPlayerTexture(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/boy.png");
            }
        }));


        ResourceManager.getInstance().setFirstPlayerShadow(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/girlShadow.png");
            }
        }));
        ResourceManager.getInstance().setSecondPlayerShadow(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/boyShadow.png");
            }
        }));

        ResourceManager.getInstance().setWallShadow(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/wallShadow.png");
            }
        }));

        ResourceManager.getInstance().setWallTexture(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/wall.png");
            }
        }));


        ResourceManager.getInstance().setBackgroundTexture(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/fon.png");
            }
        }));


        ResourceManager.getInstance().setBlockTexture(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/block1.png");
            }
        }));

        ResourceManager.getInstance().setShadowTexture(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/blockShadow.png");
            }
        }));

        ResourceManager.getInstance().setRiverTeture(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/rock.png");
            }
        }));

        ResourceManager.getInstance().setRockShadow(new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
            @Override
            public InputStream open() throws IOException {
                return activity.getAssets().open("gfx/rockShadow.png");
            }
        }));

        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getBackgroundTexture());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getFirstPlayerTexture());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getSecondPlayerTexture());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getBlockTexture());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getShadowTexture());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getWallShadow());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getWallTexture());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getFirstPlayerShadow());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getSecondPlayerShadow());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getRiverTeture());
        ResourceManager.getInstance().getTextures().add(ResourceManager.getInstance().getRockShadow());


    }

    /**
     * Завантаження всіх текстур гри
     * @param textures - список тектсур
     */
    public static void loadBitmapTextures(List<ITexture> textures) {

        for(ITexture texture: textures){
            texture.load();
        }

    }

    /**
     * Ініціалізація текстурних рамок (TextureRegions)
     */
    public static void setTextureRegions(){



        ResourceManager.getInstance().setBackgroundTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getBackgroundTexture()));
        ResourceManager.getInstance().setFirstPlayerTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getFirstPlayerTexture()));
        ResourceManager.getInstance().setSecondPlayerTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getSecondPlayerTexture()));
        ResourceManager.getInstance().setBlockTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getBlockTexture()));
        ResourceManager.getInstance().setShadowTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getShadowTexture()));
        ResourceManager.getInstance().setWallShadowTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getWallShadow()));
        ResourceManager.getInstance().setWallTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getWallTexture()));
        ResourceManager.getInstance().setFirstPlayerShadowTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getFirstPlayerShadow()));
        ResourceManager.getInstance().setSecondPlayerShadowTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getSecondPlayerShadow()));
        ResourceManager.getInstance().setRiverTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getRiverTeture()));
        ResourceManager.getInstance().setRockShadowTextureRegion(TextureRegionFactory.extractFromTexture(ResourceManager.getInstance().getRockShadow()));

    }


}
