package com.elrsoft.n_two_y_.controller;

import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.JumpModifier;

/**
 * Created by yana on 09.07.15.
 */
public class PlayerMoveController {

    /**
     * Здійснення стрибка
     *
     * @param player
     */
    public static void jump(final IEntity player) {

        final float playerSize = ResourceManager.getInstance().getPlayerWSize();

        if (player.getY() > playerSize + playerSize / 3) {

            JumpModifier jumpModifier = new JumpModifier(0.7f,
                    player.getX() - playerSize / 4, player.getX(),
                    player.getY(), player.getY() - (playerSize + playerSize/4), playerSize + playerSize/3);

            player.registerEntityModifier(jumpModifier);
        }

    }
}
