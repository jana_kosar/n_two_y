package com.elrsoft.n_two_y_.controller;

import android.util.Log;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.model.Block;
import com.elrsoft.n_two_y_.model.Obstacle;
import com.elrsoft.n_two_y_.model.River;
import com.elrsoft.n_two_y_.model.Shadow;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.entity.IEntity;
import org.andengine.entity.scene.Scene;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by yana on 09.07.15.
 */
public class SpriteController {

    /**
     * Створення та задання перешкод
     *
     * @param scene
     * @param activity
     */
    public static void drawBlock(final Scene scene, MainActivity activity) {

        ResourceManager.getInstance().setBlockSize(ResourceManager.getInstance().getWidth() / 8, ResourceManager.getInstance().getHeight() / 5);
        for (Obstacle o : getObstacleList(activity)) {

            o.setmStack(ResourceManager.getInstance().getStack());

            if (o instanceof Block) {

                o.setSize(ResourceManager.getInstance().getWidth() / 9, ResourceManager.getInstance().getHeight() / 7);

                Shadow s = new Shadow(1, o.getX(), o.getY() + o.getHeight() - o.getHeight() / 3, ResourceManager.getInstance().getShadowTextureRegion(),
                        activity.getVertexBufferObjectManager(), activity);

                s.setSize(o.getWidth(), o.getHeight() / 2);
                scene.attachChild(s);
                ResourceManager.getInstance().getStack().add(s);

            } else if (o instanceof River) {

                o.setSize(ResourceManager.getInstance().getWidth() / 3, ResourceManager.getInstance().getHeight() / 9);

                Shadow s = new Shadow(1, o.getX(), o.getY() + o.getHeight() - o.getHeight() / 3, ResourceManager.getInstance().getRockShadowTextureRegion(),
                        activity.getVertexBufferObjectManager(), activity);

                s.setSize(o.getWidth(), o.getHeight() / 2);
                scene.attachChild(s);
                ResourceManager.getInstance().getStack().add(s);

            }

            scene.attachChild(o);
            ResourceManager.getInstance().getStack().add(o);
            o.setmStack(ResourceManager.getInstance().getStack());
            scene.registerTouchArea(o);


        }
    }

    /**
     * Розміщення перешкод на ігровій сцені
     *
     * @param activity
     * @return List<Obstacle> list
     */
    public static List<Obstacle> getObstacleList(MainActivity activity) {
        ResourceManager resourceManager = ResourceManager.getInstance();

        List<Obstacle> list = new ArrayList<>();
        int x = (resourceManager.getWidth() - resourceManager.getWidth() / 8) / 6;

        switch (new Random().nextInt(6)) {

            case 0:
                list.add(new River(1, 0, new Random().nextInt(10) - 400, resourceManager.getRiverTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 1:
                list.add(new Block(1, 0, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                list.add(new Block(1, x * 2, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 2:
                list.add(new Block(1, x * 2, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 3:
                list.add(new Block(1, x, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 4:
                list.add(new Block(1, 0, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 5:
                list.add(new Block(1, x, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                list.add(new Block(1, x * 2, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 6:
                list.add(new River(1, 0, new Random().nextInt(10) - 400, resourceManager.getRiverTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                list.add(new Block(1, x, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;

        }

        int y = x * 3 + resourceManager.getWidth() / 8;

        switch (new Random().nextInt(6)) {

            case 0:
                list.add(new River(1, y, new Random().nextInt(10) - 400, resourceManager.getRiverTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 1:
                list.add(new Block(1, y, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                list.add(new Block(1, y + x * 2, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 2:
                list.add(new Block(1, y + x * 2, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 3:
                list.add(new Block(1, y + x, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 4:
                list.add(new Block(1, y, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 5:
                list.add(new Block(1, y + x, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                list.add(new Block(1, y + x * 2, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;
            case 6:
                list.add(new River(1, y, new Random().nextInt(10) - 400, resourceManager.getRiverTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                list.add(new Block(1, y + x, new Random().nextInt(10) - 400, resourceManager.getBlockTextureRegion(), activity.getVertexBufferObjectManager(), activity));
                break;

        }

        return list;

    }

    /**
     * Видалення спрайта
     *
     * @param sprite
     * @param activity
     */
    public static void removeSprite(final IEntity sprite, final MainActivity activity) {
        activity.runOnUpdateThread(new Runnable() {
            @Override
            public void run() {

                sprite.clearUpdateHandlers();
                sprite.setIgnoreUpdate(true);
                sprite.clearEntityModifiers();
                sprite.detachSelf();
                sprite.dispose();
                activity.getEngine().getScene().detachChild(sprite);

            }
        });
    }


    public static void redrawPlayers(Scene scene) {

        scene.detachChild(ResourceManager.getInstance().getLeftPlayer());
        scene.detachChild(ResourceManager.getInstance().getRightPlayer());

        scene.attachChild(ResourceManager.getInstance().getLeftPlayer());
        scene.attachChild(ResourceManager.getInstance().getRightPlayer());
    }
}
