package com.elrsoft.n_two_y_.controller;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.model.Wall;
import com.elrsoft.n_two_y_.model.WallShadow;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.entity.scene.Scene;

/**
 * Created by yana on 17.07.15.
 */
public class WallController {

    public static void createWall(final MainActivity activity, Scene scene) {

        int wallWidth = ResourceManager.getInstance().getWidth() / 8,
        wallHeight = ResourceManager.getInstance().getHeight() ;

        Wall wall = new Wall(1, ResourceManager.getInstance().getWidth() / 2 -
                wallWidth / 2, 0,
                ResourceManager.getInstance().getWallTextureRegion(), activity, scene);

        wall.setSize(wallWidth, wallHeight);
        ResourceManager.getInstance().getStack().add(wall);
        ResourceManager.getInstance().setWall(wall);

        Wall wal = new Wall(1, ResourceManager.getInstance().getWidth() / 2 -
                wallWidth / 2, 0 - wallHeight + wallHeight/9 ,
                ResourceManager.getInstance().getWallTextureRegion(), activity, scene);

        wal.setSize(wallWidth, wallHeight);
        ResourceManager.getInstance().getStack().add(wal);


        createShadowForWall(wall, scene, activity);
        scene.attachChild(wall);
        scene.attachChild(wal);
        ResourceManager.getInstance().setWall(wal);



    }

    public static void updateWall(MainActivity activity, Scene scene){

        int wallWidth = ResourceManager.getInstance().getWidth() / 8,
        height = ResourceManager.getInstance().getHeight();

        Wall wall = new Wall(1, ResourceManager.getInstance().getWidth() / 2 -
                wallWidth / 2, 0 - height,
                ResourceManager.getInstance().getWallTextureRegion(), activity, scene);

        wall.setSize(wallWidth, height);
        ResourceManager.getInstance().getStack().add(wall);

        scene.attachChild(wall);
        ResourceManager.getInstance().setWall(wall);

    }


    public static void createShadowForWall(Wall wall, Scene scene, MainActivity activity){


        int wallWidth = ResourceManager.getInstance().getWidth() / 8,
                wallHeight = ResourceManager.getInstance().getHeight() ;

        WallShadow wallShadow = new WallShadow(1, wall.getX() + wall.getWidth() - wall.getWidth()/3, 0,
                ResourceManager.getInstance().getWallShadowTextureRegion(),scene, activity);

        wallShadow.setSize(wall.getWidth() / 2, wallHeight);
        ResourceManager.getInstance().getStack().add(wallShadow);
        scene.attachChild(wallShadow);

        WallShadow wallShadow1 = new WallShadow(1,   wall.getX() + wall.getWidth() - wall.getWidth()/3, 0 - wallHeight ,
                ResourceManager.getInstance().getWallShadowTextureRegion(), scene, activity);

        wallShadow1.setSize(wallWidth/2, wallHeight + 3);
        ResourceManager.getInstance().getStack().add(wallShadow1);
        scene.attachChild(wallShadow1);

    }

    public static void updateShadow(MainActivity activity, Scene scene){

        int wallWidth = ResourceManager.getInstance().getWidth() / 8,
                height = ResourceManager.getInstance().getHeight();
        Wall wal = ResourceManager.getInstance().getWall();

        WallShadow wall = new WallShadow(1,  wal.getX() + wal.getWidth() - wal.getWidth()/3, 0 - height,
                ResourceManager.getInstance().getWallShadowTextureRegion(), scene, activity);


        wall.setSize(wallWidth/2, height + 3);
        ResourceManager.getInstance().getStack().add(wall);
        scene.attachChild(wall);


    }

}
