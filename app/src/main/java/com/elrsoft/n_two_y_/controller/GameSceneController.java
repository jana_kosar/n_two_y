package com.elrsoft.n_two_y_.controller;

import android.util.Log;
import android.view.MotionEvent;

import com.elrsoft.n_two_y_.activity.MainActivity;
import com.elrsoft.n_two_y_.model.Player;
import com.elrsoft.n_two_y_.model.Wall;
import com.elrsoft.n_two_y_.resource.ResourceManager;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;

/**
 * Created by yana on 09.07.15.
 */
public class GameSceneController implements IOnSceneTouchListener {

    private float y_touch_coordinate, x_touch_coordinate;
//    boolean isTousch = false;

    /**
     * Створення ігрової сцени
     *
     * @param activity - activity ігрової сцениж
     * @return об'єкт scene;
     */
    public Scene onCreateScene(final MainActivity activity) {

        final Scene scene = new Scene();

        Sprite sceneBackground = new Sprite(0, 0, ResourceManager.getInstance().getBackgroundTextureRegion(), activity.getVertexBufferObjectManager());
        scene.attachChild(sceneBackground);
        sceneBackground.setSize(ResourceManager.getInstance().getWidth(), ResourceManager.getInstance().getHeight());

        createLeftPlayerSprite(activity, scene);
        WallController.createWall(activity, scene);
        createRightPlayerSprite(activity, scene);

        scene.setTouchAreaBindingOnActionDownEnabled(true);
        scene.setOnSceneTouchListener(this);

        scene.registerUpdateHandler(new IUpdateHandler() {

            @Override
            public void reset() {
            }

            @Override
            public void onUpdate(final float pSecondsElapsed) {

                if (ResourceManager.getInstance().isTouch() && scene.getChildByIndex(scene.getChildCount() - 5).getY() > 60) {

                    SpriteController.drawBlock(scene, activity);
                    SpriteController.redrawPlayers(scene);
                }
            }
        });
        return scene;
    }


    /**
     * Створення, ініціалізація та накладання слухача для  лівого гравця
     *
     * @param activity - activity, якому належить сцена з гравцем
     * @param scene    - scene, якій належить гравець
     */
    public void createLeftPlayerSprite(final MainActivity activity, Scene scene) {


        final Player leftPlayer = new Player(1, ResourceManager.getInstance().getWidth() / 4,
                ResourceManager.getInstance().getHeight() - ResourceManager.getInstance().getHeight() / 4,
                ResourceManager.getInstance().getFirstPlayerTextureRegion(), activity.getVertexBufferObjectManager()) {

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

//
//                if ((this.getmStack().peek()) instanceof Player && ((Player) this.getmStack().peek()).getmWeight() != this.getmWeight()) {
//                    return false;
//                }

                if (pSceneTouchEvent.getY() <= ResourceManager.getInstance().getHeight() - getHeight() / 2
                        && pSceneTouchEvent.getY() >= 0 + getHeight() / 2 && pSceneTouchEvent.getX() >= 0 + getWidth() / 2
                        && pSceneTouchEvent.getX() <= ResourceManager.getInstance().getWidth() / 2 - getWidth() / 2) {

                    this.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2,
                            pSceneTouchEvent.getY() - this.getHeight() / 2);
                    ResourceManager.getInstance().setIsTouch(true);

                }

                return true;
            }


        };


        ResourceManager.getInstance().setPlayerSize(ResourceManager.getInstance().getHeight() / 8 + ResourceManager.getInstance().getHeight() / 30, ResourceManager.getInstance().getWidth() / 8);
        leftPlayer.setSize(ResourceManager.getInstance().getHeight() / 8 + ResourceManager.getInstance().getHeight() / 30, ResourceManager.getInstance().getWidth() / 8);
        leftPlayer.setmStack(ResourceManager.getInstance().getStack());

        ResourceManager.getInstance().getStack().add(leftPlayer);
        ResourceManager.getInstance().setLeftPlayer(leftPlayer);

        scene.registerTouchArea(leftPlayer);

        createShadowForPlayer(leftPlayer, scene, activity, ResourceManager.getInstance().getFirstPlayerShadowTextureRegion());

        leftPlayer.registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {

                leftPlayer.getShadow().setPosition(leftPlayer.getX()+leftPlayer.getWidth()/9, leftPlayer.getY()+leftPlayer.getHeight() - leftPlayer.getHeight()/8);

                if (leftPlayer.getX() + ResourceManager.getInstance().getPlayerWSize() >= ResourceManager.getInstance().getWall().getX()) {

                    leftPlayer.setPosition(ResourceManager.getInstance().getWall().getX() - ResourceManager.getInstance().getPlayerWSize(),
                            leftPlayer.getY());
                }
            }

            @Override
            public void reset() {

            }
        });

        scene.attachChild(leftPlayer);

    }


    public void createRightPlayerSprite(final MainActivity activity, Scene scene) {

        final Player rightPlayer = new Player(1, ResourceManager.getInstance().getWidth() / 4 * 3,
                ResourceManager.getInstance().getHeight() - ResourceManager.getInstance().getHeight() / 4,
                ResourceManager.getInstance().getSecondPlayerTextureRegion(), activity.getVertexBufferObjectManager()) {

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

                if (pSceneTouchEvent.getY() <= ResourceManager.getInstance().getHeight() - getHeight() / 2
                        && pSceneTouchEvent.getY() >= 0 + getHeight() / 2
                        && pSceneTouchEvent.getX() >= ResourceManager.getInstance().getWidth() / 2 + getWidth() / 2
                        && pSceneTouchEvent.getX() <= ResourceManager.getInstance().getWidth() - getWidth() / 2) {

                    this.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2,
                            pSceneTouchEvent.getY() - this.getHeight() / 2);
                    ResourceManager.getInstance().setIsTouch(true);


                }
                return true;

            }


        };

        rightPlayer.setSize(ResourceManager.getInstance().getHeight() / 8 + ResourceManager.getInstance().getHeight() / 30, ResourceManager.getInstance().getWidth() / 8);
        rightPlayer.setmStack(ResourceManager.getInstance().getStack());

        scene.registerTouchArea(rightPlayer);

        ResourceManager.getInstance().getStack().add(rightPlayer);
        ResourceManager.getInstance().setRightPlayer(rightPlayer);

        createShadowForPlayer(rightPlayer, scene, activity, ResourceManager.getInstance().getSecondPlayerShadowTextureRegion());

        rightPlayer.registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {

                rightPlayer.getShadow().setPosition(rightPlayer.getX() + rightPlayer.getWidth()/9, rightPlayer.getY() + rightPlayer.getHeight() - rightPlayer.getHeight()/8);

                if (rightPlayer.getX() <= ResourceManager.getInstance().getWall().getX() + ResourceManager.getInstance().getWall().getWidth()) {

                    rightPlayer.setPosition(ResourceManager.getInstance().getWall().getX() + ResourceManager.getInstance().getWall().getWidth(), rightPlayer.getY());
                }
            }

            @Override
            public void reset() {

            }
        });

        scene.attachChild(rightPlayer);

    }


    /**
     * Слухач взаємодії користустувача зі "сценою", при свайпі з низу до верху відбувається стрибок гравця
     *
     * @param pScene The {@link Scene} that the {@link TouchEvent} has been dispatched to.
     * @param event
     * @return
     */
    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent event) {

        switch (event.getAction()) {

            //при взаємодії користувача зі сценою, зберігаються координати початку цієї взаємодії
            case MotionEvent.ACTION_DOWN:
                y_touch_coordinate = event.getY();
                x_touch_coordinate = event.getX();
                break;

            case MotionEvent.ACTION_UP:
                //після завершення взаємодії порівнюються координати, вичислюючи верхній свайп
                if (Math.abs(event.getY() - y_touch_coordinate) > 90 && Math.abs(x_touch_coordinate - event.getX()) < 180) {

                    if (y_touch_coordinate > event.getY()) {
                        ResourceManager.getInstance().setIsTouch(true);

                        if (x_touch_coordinate >= 0 && x_touch_coordinate <= ResourceManager.getInstance().getWidth() / 2) {

                            PlayerMoveController.jump(ResourceManager.getInstance().getLeftPlayer());
                        }
                        if (x_touch_coordinate > ResourceManager.getInstance().getWidth() / 2
                                && x_touch_coordinate <= ResourceManager.getInstance().getWidth()) {

                            PlayerMoveController.jump(ResourceManager.getInstance().getRightPlayer());

                        }
                    }

                }

                break;
        }

        return true;
    }

    public static void createShadowForPlayer(Player player, Scene scene, MainActivity activity, ITextureRegion playerShadow) {

        Sprite shadow = new Sprite(player.getX() + player.getWidth()/11, player.getY() + player.getHeight() - player.getHeight()/8, playerShadow,
                activity.getVertexBufferObjectManager());

        player.setShadow(shadow);
        shadow.setSize(player.getWidth() + player.getWidth()/4, player.getHeight()/2);

        scene.attachChild(shadow);
        scene.registerTouchArea(shadow);
        ResourceManager.getInstance().getStack().add(shadow);

    }


}
