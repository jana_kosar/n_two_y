package com.elrsoft.n_two_y_;

import com.elrsoft.n_two_y_.activity.MainActivity;

/**
 * Created by yana on 17.06.15.
 */
public class ResourceManager {

    private static final ResourceManager INSTANCE = new ResourceManager();

    public MainActivity mActivity;

    private ResourceManager() {}

    public static ResourceManager getInstance() {
        return INSTANCE;
    }

    public void prepare(MainActivity activity) {
        INSTANCE.mActivity = activity;
    }

    public void loadSplashResources() {
        //TODO implement
    }

    public void unloadSplashResources() {
        //TODO implement
    }

    public void loadGameResources() {
        //TODO implement
    }

    public void unloadGameResources() {
        //TODO implement
    }

}
