//package com.elrsoft.n_two_y_;
//
//
//import android.content.Context;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.Display;
//import android.view.MotionEvent;
//import android.view.WindowManager;
//
//import com.elrsoft.n_two_y_.model.Obstacle;
//import com.elrsoft.n_two_y_.model.Player;
//import com.elrsoft.n_two_y_.model.Block;
//import com.elrsoft.n_two_y_.model.River;
//
//import org.andengine.engine.camera.Camera;
//import org.andengine.engine.handler.IUpdateHandler;
//import org.andengine.engine.handler.physics.PhysicsHandler;
//import org.andengine.engine.handler.timer.ITimerCallback;
//import org.andengine.engine.handler.timer.TimerHandler;
//import org.andengine.engine.options.EngineOptions;
//import org.andengine.engine.options.ScreenOrientation;
//import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
//
//import org.andengine.entity.IEntity;
//
//import org.andengine.entity.modifier.JumpModifier;
//import org.andengine.entity.modifier.MoveYModifier;
//import org.andengine.entity.modifier.SequenceEntityModifier;
//import org.andengine.entity.scene.IOnAreaTouchListener;
//import org.andengine.entity.scene.IOnSceneTouchListener;
//import org.andengine.entity.scene.ITouchArea;
//import org.andengine.entity.scene.Scene;
//import org.andengine.entity.sprite.Sprite;
//import org.andengine.input.sensor.acceleration.AccelerationData;
//import org.andengine.input.sensor.acceleration.IAccelerationListener;
//import org.andengine.input.touch.TouchEvent;
//import org.andengine.opengl.texture.ITexture;
//import org.andengine.opengl.texture.bitmap.BitmapTexture;
//import org.andengine.opengl.texture.region.ITextureRegion;
//import org.andengine.opengl.texture.region.TextureRegionFactory;
//import org.andengine.ui.activity.SimpleBaseGameActivity;
//import org.andengine.util.adt.io.in.IInputStreamOpener;
//import org.andengine.util.debug.Debug;
//import org.andengine.util.modifier.IModifier;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Random;
//import java.util.Stack;
//
//public class MainActivity extends SimpleBaseGameActivity {
//
//    private ITextureRegion mBackgroundTextureRegion, mRing11, mRing12, mRing13;
//    private Stack mStack1;
//    ITexture ring11, ring13;
//    ITexture ring12;
//    ITexture backgroundTexture;
//
//    boolean b = false, isJumping;
//    int width;
//    int height;
//
//    private int jumpHeight = 100;
//
//    long beginTime = 0, endTime = 0;
//    float startPosition;
//
//    @Override
//    protected void onCreateResources() {
//
//
//        try {
//            // 1 - Set up bitmap textures
//            backgroundTexture = new BitmapTexture(this.getTextureManager(), new IInputStreamOpener() {
//                @Override
//                public InputStream open() throws IOException {
//                    return getAssets().open("gfx/texture.jpg");
//                }
//            });
//
//            ring11 = new BitmapTexture(this.getTextureManager(), new IInputStreamOpener() {
//                @Override
//                public InputStream open() throws IOException {
//                    return getAssets().open("gfx/squre.png");
//                }
//            });
//
//            ring12 = new BitmapTexture(this.getTextureManager(), new IInputStreamOpener() {
//                @Override
//                public InputStream open() throws IOException {
//                    return getAssets().open("gfx/squre.png");
//                }
//            });
//
//            ring13 = new BitmapTexture(this.getTextureManager(), new IInputStreamOpener() {
//                @Override
//                public InputStream open() throws IOException {
//                    return getAssets().open("gfx/squre.png");
//                }
//            });
//
//            // 2 - Load bitmap textures into VRAM
//            backgroundTexture.load();
//            ring11.load();
//            ring12.load();
//            ring13.load();
//
//        } catch (IOException e) {
//            Debug.e(e);
//        }
//        // 3 - Set up texture regions
//        this.mBackgroundTextureRegion = TextureRegionFactory.extractFromTexture(backgroundTexture);
//        this.mRing11 = TextureRegionFactory.extractFromTexture(ring11);
//        this.mRing12 = TextureRegionFactory.extractFromTexture(ring12);
//        this.mRing13 = TextureRegionFactory.extractFromTexture(ring13);
//
//        // 4 - Create the stacks
//        this.mStack1 = new Stack();
//    }
//
//
//    @Override
//    protected Scene onCreateScene() {
//        // 1 - Create new scene
//        final Scene scene = new Scene();
//        Sprite backgroundSprite = new Sprite(0, 0, this.mBackgroundTextureRegion, getVertexBufferObjectManager());
//        scene.attachChild(backgroundSprite);
//
//        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        DisplayMetrics metrics = new DisplayMetrics();
//        display.getMetrics(metrics);
//        int width = metrics.widthPixels;
//        final int height = metrics.heightPixels;
//
//
//        final Player ring11 = new Player(1, width / 4, height - height / 4, this.mRing11, getVertexBufferObjectManager()) {
//            @Override
//            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
//
//                beginTime = new Date().getTime();
//                startPosition = this.getY();
//
//                Log.d("jump", "locatTouchY = " +pTouchAreaLocalY + " currentY = " + getY());
//                if (pSceneTouchEvent.isActionUp()) {
//
//                    jump(this, pSceneTouchEvent);
//
//
//
//
//                } else if (!isJumping && pSceneTouchEvent.isActionMove()) {
//
//                    b = true;
//                    WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
//                    Display display = wm.getDefaultDisplay();
//                    DisplayMetrics metrics = new DisplayMetrics();
//                    display.getMetrics(metrics);
//                    int width = metrics.widthPixels;
//                    int height = metrics.heightPixels;
//
//                    Log.d("MyLog", width + " (--------) " + height);
//
//                    if ((this.getmStack().peek()) instanceof Player && ((Player) this.getmStack().peek()).getmWeight() != this.getmWeight()) {
//                        return false;
//                    }
//
//                    Log.d("position", "X = " + pSceneTouchEvent.getX() + " Y = " + pSceneTouchEvent.getY());
//                    if (pSceneTouchEvent.getY() <= height - getHeight() / 2 && pSceneTouchEvent.getY() >= 0 + getHeight() / 2 && pSceneTouchEvent.getX() >= 0 + getWidth() / 2 && pSceneTouchEvent.getX() <= width / 2 - getWidth() / 2) {
//                        this.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2,
//                                pSceneTouchEvent.getY() - this.getHeight() / 2);
//
//                    }
//
//                }
//
//                return true;
//            }
//
//
//
//
//        };
//
//
//        Player ring12 = new Player(1, width / 4 * 3, height - height / 4, this.mRing12, getVertexBufferObjectManager()) {
//            @Override
//            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
//
//                b = true;
//                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
//                Display display = wm.getDefaultDisplay();
//                DisplayMetrics metrics = new DisplayMetrics();
//                display.getMetrics(metrics);
//                int width = metrics.widthPixels;
//                int height = metrics.heightPixels;
//
//
//                if ((this.getmStack().peek()) instanceof Player && ((Player) this.getmStack().peek()).getmWeight() != this.getmWeight()) {
//                    return false;
//                }
//
//                Log.d("position", "X = " + pSceneTouchEvent.getX() + " Y = " + pSceneTouchEvent.getY());
//                if (pSceneTouchEvent.getY() <= height - getHeight() / 2 && pSceneTouchEvent.getY() >= 0 + getHeight() / 2 && pSceneTouchEvent.getX() >= width / 2 + getWidth() / 2 && pSceneTouchEvent.getX() <= width - getWidth() / 2) {
//                    this.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2,
//                            pSceneTouchEvent.getY() - this.getHeight() / 2);
//
//                }
////
////                if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
////                }
//
//                return true;
//
//            }
//
//            @Override
//            public void onManagedUpdate(float pSecondsElapsed) {
//                Log.d("count", scene.getChildCount() + ", ");
//                if (b && scene.getChildByIndex(scene.getChildCount() - 1).getY() > 60) {
//
////                    drawBlock(scene);
//
//                }
//
//
//            }
//
//        };
//
//
//        scene.attachChild(ring11);
//        scene.attachChild(ring12);
//
//        this.mStack1.add(ring11);
//        this.mStack1.add(ring12);
//
//        ring11.setmStack(mStack1);
//        ring12.setmStack(mStack1);
//
//        scene.registerTouchArea(ring11);
//        scene.registerTouchArea(ring12);
//
//        scene.setTouchAreaBindingOnActionDownEnabled(true);
//        return scene;
//    }
//
//    @Override
//    public EngineOptions onCreateEngineOptions() {
//
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        width = metrics.widthPixels;
//        height = metrics.heightPixels;
//        final Camera camera = new Camera(0, 0, width, height);
//
//
//        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
//                new RatioResolutionPolicy(width, height), camera);
//        engineOptions.getTouchOptions().setNeedsMultiTouch(true);
//
//
//        return engineOptions;
//    }
//
//
//    public boolean jump(final IEntity player, TouchEvent touchEvent) {
//
//        float start, finish;
//        long startTime = 0, finishTime = 0;
//
//            Log.d("jumping", "start = "  + beginTime + " position = " + startPosition);
//            finish = player.getY();
//            finishTime = new Date().getTime();
//            Log.d("jumping", "finish = "  + finishTime + " endPosition = " + finish);
//
//
//
//        Log.d("jumping", "diff = "  + (finishTime - beginTime));
//
//
//        if(finishTime - beginTime <= 1 && finishTime - beginTime > 0) {
//            final JumpModifier jumpModifier = new JumpModifier(1, player.getX(), player.getX() + 50,
//                    player.getY(), player.getY() - jumpHeight, jumpHeight);
//
//            player.registerEntityModifier(jumpModifier);
//        }
//
//
//        return true;
//    }
//
//
//
//
//    public void drawBlock(final Scene scene) {
//
//
//        for (Obstacle o : getObstacleList()) {
//
//            scene.attachChild(o);
//            this.mStack1.add(o);
//            o.setmStack(mStack1);
//            scene.registerTouchArea(o);
//
//        }
//    }
//
//
//    public void removeSprite(final IEntity sprite) {
//        runOnUpdateThread(new Runnable() {
//            @Override
//            public void run() {
//
//                sprite.clearUpdateHandlers();
//                sprite.setIgnoreUpdate(true);
//                sprite.clearEntityModifiers();
//                sprite.detachSelf();
//                sprite.dispose();
//                getEngine().getScene().detachChild(sprite);
//
//            }
//        });
//    }
//
//
//    public List<Obstacle> getObstacleList() {
//
//        List<Obstacle> list = new ArrayList<>();
//        int x = (width - width / 8) / 6;
//
//        switch (new Random().nextInt(6)) {
//
//            case 0:
//                list.add(new River(1, 0, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 1:
//                list.add(new Block(1, 0, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                list.add(new Block(1, x * 2, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 2:
//                list.add(new Block(1, x * 2, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 3:
//                list.add(new Block(1, x, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 4:
//                list.add(new Block(1, 0, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 5:
//                list.add(new Block(1, x, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                list.add(new Block(1, x * 2, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 6:
//                list.add(new River(1, 0, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                list.add(new Block(1, x, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//
//        }
//
//        int y = x * 3 + width / 8;
//
//        switch (new Random().nextInt(6)) {
//
//            case 0:
//                list.add(new River(1, y, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 1:
//                list.add(new Block(1, y, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                list.add(new Block(1, y + x * 2, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 2:
//                list.add(new Block(1, y + x * 2, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 3:
//                list.add(new Block(1, y + x, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 4:
//                list.add(new Block(1, y, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 5:
//                list.add(new Block(1, y + x, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                list.add(new Block(1, y + x * 2, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//            case 6:
//                list.add(new River(1, y, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                list.add(new Block(1, y + x, new Random().nextInt(10) - 60, this.mRing12, getVertexBufferObjectManager(), this));
//                break;
//
//        }
//
//        return list;
//
//    }
//
//
//}
